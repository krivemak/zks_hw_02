# zks__hw02

To setup this project:

## 1. Clone this repo before and go to installed folder:

```git clone https://gitlab.fel.cvut.cz/krivemak/zks_hw_02.git```

```cd zks__hw02/```

## 2. Run composer with ToDoMVC on board, to make server alive:

```docker-compose up -d```

Now ToDoMVC application will be available by address ```localhost:21095```

To stop the server use following command:

```docker-compose down```


## 3. Install our test environment (npm + Cypress.io) by installing npm, which will take ```package.json``` with dependencies:

```npm install```

Now you may make a cup of coffee. Installing will take a couple of minutes (depends on your Internet connection).

## 4. Use Cypress in 2 modes:
 
- ```./node_modules/.bin/cypress run``` \- via CLI

CLI version will run needed test, which is located here: /cypress/integration/zks_hw02.js

- ```./node_modules/.bin/cypress open``` \- via GUI

In GUI version needed to run zks_hw02.js manually, by clicking on it.

**Note!!** In case of any troubles at "BEFORE ALL" step at first test, try to remove all screenshots inside the ```cypress/integration/__image_snapshots__``` folder.

_______________

#### Screenshots will be here:
- [cypress/integration/\_\_image_snapshots\_\_]()- this folder created by [**cypress-plugin-snapshots**](https://www.npmjs.com/package/cypress-plugin-snapshots)  plugin
The diff image will be here too.

- [cypress/screenshots/zks_hw02.js]() - this one created by defauld [`.screenshot()`](https://docs.cypress.io/api/commands/screenshot.html) command

#### Video is here:
- [cypress/videos/](/cypress/videos)
![video of test](/cypress/videos/zks_hw02.js.mp4)
#### My diff screenshots are here:
- [cypress/integration/\_image_snapshots\_](tree/master/cypress/integration/_image_snapshots_)

![img](https://gitlab.fel.cvut.cz/krivemak/zks_hw_02/raw/master/cypress/integration/_image_snapshots_/ToDo%20list%20items%20%20change%20some%20css%20properties%20and%20complete%203rd%20item%20%230.diff.png)

